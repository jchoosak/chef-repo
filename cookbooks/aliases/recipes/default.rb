#
# Cookbook Name:: aliases
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
# Alias `h` to go home
magic_shell_alias 'h' do
command 'cd ~'
end

# Alias `sties` to cd into apache
magic_shell_alias 'sites' do
command "cd #{node['apache']['dir']}/sites-enabled"
end

# Set VI as the default editor
magic_shell_environment 'EDITOR' do
value 'vi'
end


# Set ll
magic_shel_alias 'll' do
command "ls -l"
end
